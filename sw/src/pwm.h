#ifndef __PWM_H_
#define __PWM_H_

#include <libopencm3/stm32/gpio.h>
#include <libopencm3/stm32/rcc.h>
#include <libopencm3/stm32/timer.h>

#include "globales.h"

#define vbase  3000

void giro_busqueda(void);
void velocidad_motor(void);
void Atras(void);
void Giro_amplio_CW(void);
void Giro_amplio_CCW(void);
#endif