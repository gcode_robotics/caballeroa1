

#include "setup.h"
#include "globales.h"



void setup_PWM_timer(void) {
  
  timer_set_mode(TIM4, TIM_CR1_CKD_CK_INT, TIM_CR1_CMS_EDGE, TIM_CR1_DIR_UP);
  /////
  timer_set_prescaler(TIM4, 30); // es aqui donde modificamos el periodo, lo compruebo con el oscilo, esta para un duty 2 ms
  timer_set_repetition_counter(TIM4, 0);
  timer_enable_preload(TIM4);
  timer_continuous_mode(TIM4);
  timer_set_period(TIM4, 4500);

//////

  timer_set_oc_mode(TIM4, TIM_OC1, TIM_OCM_PWM1);
  //timer_set_oc_value(TIM4, TIM_OC1, 4500);
  timer_enable_oc_output(TIM4, TIM_OC1);
  
  timer_set_oc_mode(TIM4, TIM_OC2, TIM_OCM_PWM1);
  //timer_set_oc_value(TIM4, TIM_OC2, 4500);
  timer_enable_oc_output(TIM4, TIM_OC2);
  
  timer_set_oc_mode(TIM4, TIM_OC3, TIM_OCM_PWM1);
  //timer_set_oc_value(TIM4, TIM_OC3, 4500);
  timer_enable_oc_output(TIM4, TIM_OC3);
  
  timer_set_oc_mode(TIM4, TIM_OC4, TIM_OCM_PWM1);
  //timer_set_oc_value(TIM4, TIM_OC4, 4500);
  timer_enable_oc_output(TIM4, TIM_OC4);
  
  timer_enable_counter(TIM4);
  }


void setup_clocks(void){
  // __CONFIGURACIÓN DEl MICRO__ //
  // Establece la velocidad del reloj en 72mhz
  rcc_clock_setup_in_hse_8mhz_out_72mhz();
  // Habilita el contador de ticks de reloj
  dwt_enable_cycle_counter();
  // Inicia el reloj 
  rcc_periph_clock_enable(RCC_GPIOB);
  rcc_periph_clock_enable(RCC_TIM4);
 
  // Establece el pin todos los pines que pides a PWM...
   gpio_set_mode(GPIOB, GPIO_MODE_OUTPUT_50_MHZ, GPIO_CNF_OUTPUT_ALTFN_PUSHPULL, GPIO_TIM4_CH1);
   gpio_set_mode(GPIOB, GPIO_MODE_OUTPUT_50_MHZ, GPIO_CNF_OUTPUT_ALTFN_PUSHPULL, GPIO_TIM4_CH2);
   gpio_set_mode(GPIOB, GPIO_MODE_OUTPUT_50_MHZ, GPIO_CNF_OUTPUT_ALTFN_PUSHPULL, GPIO_TIM4_CH3);
   gpio_set_mode(GPIOB, GPIO_MODE_OUTPUT_50_MHZ, GPIO_CNF_OUTPUT_ALTFN_PUSHPULL, GPIO_TIM4_CH4);
  
  // Inicia los relojes del TIM4
  rcc_periph_clock_enable(RCC_TIM4);
  }

  void setup_gpio(void){
      rcc_periph_clock_enable(RCC_GPIOB);
      rcc_periph_clock_enable(RCC_GPIOA);
      rcc_periph_clock_enable(RCC_AFIO);

       AFIO_MAPR|=AFIO_MAPR_SWJ_CFG_JTAG_OFF_SW_OFF;

    // Entradas digitales configuracion
    gpio_set_mode(GPIOB, GPIO_MODE_INPUT, GPIO_CNF_INPUT_PULL_UPDOWN, GPIO13|GPIO14);
    //Esto ADC solo esta aqui pa no olvidarme
    //gpio_set_mode(GPIOA, GPIO_MODE_INPUT, GPIO_CNF_INPUT_PULL_UPDOWN, GPIO0|GPIO1);
    //salidas digitales
    gpio_set_mode(GPIOA, GPIO_MODE_OUTPUT_50_MHZ, GPIO_CNF_OUTPUT_PUSHPULL, GPIO8|GPIO11|GPIO12|GPIO15);
    gpio_set_mode(GPIOB, GPIO_MODE_OUTPUT_50_MHZ, GPIO_CNF_OUTPUT_PUSHPULL, GPIO3|GPIO4|GPIO5|GPIO10|GPIO11|GPIO15);

  }


// Sensor's buffer 
// Must be volatile so that the value is updated every time it's consulted 


void usart_setup(void)
{
	/* Enable clocks for GPIO port B (for GPIO_USART1_TX) and USART1. */
	rcc_periph_clock_enable(RCC_GPIOA);
	rcc_periph_clock_enable(RCC_USART1);

	/* Setup GPIO pin GPIO_USART1_TX/PB10 on GPIO port B for transmit. */
	gpio_set_mode(GPIOA, GPIO_MODE_OUTPUT_50_MHZ, GPIO_CNF_OUTPUT_ALTFN_PUSHPULL, GPIO_USART1_TX);
  gpio_set_mode(GPIOA, GPIO_MODE_OUTPUT_50_MHZ, GPIO_CNF_OUTPUT_PUSHPULL, GPIO5|GPIO6|GPIO7|GPIO12);
	/* Setup USART parameters. */
	usart_set_baudrate(USART1, 9600);
	usart_set_databits(USART1, 16	);
	usart_set_stopbits(USART1, USART_STOPBITS_1);
	usart_set_mode(USART1, USART_MODE_TX_RX);
	usart_set_parity(USART1, USART_PARITY_NONE);
	usart_set_flow_control(USART1, USART_FLOWCONTROL_NONE);

	/* Finally enable the USART. */
	usart_enable(USART1);
}

static void adc_setup(void)
{
	int i;
	
  // This ADC configuration has to be inizialiced by software
  // to convert a secuente of channels automaticaly. 
  // This ADC it is syncronize with the DMA, wich means that the 
  // DMA write the converted values on a memory address asigned

	// Enable ADC1 Clock
	rcc_periph_clock_enable(RCC_ADC1);

	// Make sure the ADC doesn't run during config
	adc_off(ADC1);

	// We configure everything for one scan conversion
	adc_enable_scan_mode(ADC1);
	adc_set_continuous_conversion_mode(ADC1);
	adc_disable_external_trigger_regular(ADC1);
	adc_set_right_aligned(ADC1);

	adc_set_sample_time_on_all_channels(ADC1, ADC_SMPR_SMP_1DOT5CYC);
	adc_power_on(ADC1);

	// Wait for ADC starting up
	for (i = 0; i < 800000; i++)    // Wait a bit
		__asm__("nop");

	adc_reset_calibration(ADC1);
	adc_calibration(ADC1);

	// Channels to scan : IR matrix
	uint8_t channels[NUM_SENSORS] = {ADC_CHANNEL0, ADC_CHANNEL1};
	// Sets the scanning sequence 
	adc_set_regular_sequence(ADC1, NUM_SENSORS, channels);

}

static void setup_dma_adc1() {
  // Enables DMA1 Clock
  rcc_periph_clock_enable(RCC_DMA1);
  // Resets the channel and clears the registers before configuration
  dma_channel_reset(DMA1, DMA_CHANNEL1);
  // Sets the ADC peripheral address
  // (uint32_t)&ADC_DR(ADC1) represents the ADC1 address 
  dma_set_peripheral_address(DMA1, DMA_CHANNEL1, (uint32_t)&ADC_DR(ADC1));
  // Sets the base memory address assigned to the peripheral's channel
  dma_set_memory_address(DMA1, DMA_CHANNEL1, (uint32_t)&sensors);
  // Enables the memory to increment its pointer according to the data size
  // after each write operation
  // This does not change the base memory address
  dma_enable_memory_increment_mode(DMA1, DMA_CHANNEL1);
  // Peripheral data size (8, 16, 32) (word size)
  dma_set_peripheral_size(DMA1, DMA_CHANNEL1, DMA_CCR_PSIZE_16BIT);
  // Memory data size (8, 16, 32) (word size)
  dma_set_memory_size(DMA1, DMA_CHANNEL1, DMA_CCR_MSIZE_16BIT);
  // Channel priority from LOW to VERY_HIGH
  dma_set_priority(DMA1, DMA_CHANNEL1, DMA_CCR_PL_HIGH);

  // Sets the size of the memory block in words
  dma_set_number_of_data(DMA1, DMA_CHANNEL1, NUM_SENSORS);
  // Enables the dma to write over the first word in the base 
  // memory address after it has reached the end of the memory block
  dma_enable_circular_mode(DMA1, DMA_CHANNEL1);
  // Sets transfer direction peripheral to memory
  dma_set_read_from_peripheral(DMA1, DMA_CHANNEL1);
  // Associates the channel to the stream
  // Enables the stream
  dma_enable_channel(DMA1, DMA_CHANNEL1);
  // Enables dma transfers
  adc_enable_dma(ADC1);
}

void my_usart_print_int(uint32_t usart, int value)
//The function usart_send_blocking works char by char
//This function converts the integer to a char matix and sends
//the elements sequentially
{
	int8_t i;
	uint8_t nr_digits = 0;
	char buffer[25];

	//If the integer is a negative number a '-' sign is sent and the value is
	//converted to positive 
	if (value < 0) {
		usart_send_blocking(usart, '-');
		value = value * -1;
	}

	//The value is translated to the char matrix
	while (value > 0) {
		//The last number (units) of the value is saved in the char matrix
		//It is calculated as the remainder (mod %) dividing by 10
		//The index of the matrix is post-incremented
		buffer[nr_digits++] = "0123456789"[value % 10];
		//The value is divided by 10 to find the next number
		value /= 10;
	}

	for (i = (nr_digits - 1); i >= 0; i--) {
		//The values are sent last to first so that the value is read left to right
		usart_send_blocking(usart, buffer[i]);
	}

	usart_send_blocking(usart, '\t');
}





  void setup(void){

    setup_clocks();
    setup_gpio();
    usart_setup();
    adc_setup();
    setup_dma_adc1();  
    setup_PWM_timer();
    
        //START THE ADC SECUENCE CONVERSION
	  adc_start_conversion_direct(ADC1);
  }