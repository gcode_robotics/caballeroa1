// clang-format off
#include "pwm.h"
// clang-format on

void giro_busqueda(void){
// para el duty hay que moverse entre los valores de 2250 0% Duty y 4800 100% duty

int velocidad_iz=2250+1000; //pongo 0 para indicar el incremento que no exceda los 4800 ojo
int velocidad_der=2250+1000; 
timer_set_oc_value(TIM4, TIM_OC1, velocidad_iz);
timer_set_oc_value(TIM4, TIM_OC2, velocidad_der);
timer_set_oc_value(TIM4, TIM_OC3, velocidad_iz);
timer_set_oc_value(TIM4, TIM_OC4, velocidad_der);

}

void velocidad_motor(void){
// aqui tiene que ir el PID donde varia el duty cicle
// para el duty hay que moverse entre los valores de 2250 0% Duty y 4800 100% duty
//despues de todo el calculo se modificara el int para cargarlo en el motor

timer_set_oc_value(TIM4, TIM_OC1, vbase);
timer_set_oc_value(TIM4, TIM_OC2, vbase);
timer_set_oc_value(TIM4, TIM_OC3, vbase);
timer_set_oc_value(TIM4, TIM_OC4, vbase);


}

void Atras(void){

// para el duty hay que moverse entre los valores de 2250 0% Duty y 4800 100% duty

timer_set_oc_value(TIM4, TIM_OC1, vbase);
timer_set_oc_value(TIM4, TIM_OC2, vbase);
timer_set_oc_value(TIM4, TIM_OC3, vbase);
timer_set_oc_value(TIM4, TIM_OC4, vbase);


}
void Giro_amplio_CW(void){

// para el duty hay que moverse entre los valores de 2250 0% Duty y 4800 100% duty

int velocidad_iz=2250+2000; //pongo 0 para indicar el incremento que no exceda los 4800 ojo
int velocidad_der=2250+1000; //pongo 0 para indicar el incremento que no exceda los 4800 ojo
timer_set_oc_value(TIM4, TIM_OC1, velocidad_iz);
timer_set_oc_value(TIM4, TIM_OC2, velocidad_der);
timer_set_oc_value(TIM4, TIM_OC3, velocidad_iz);
timer_set_oc_value(TIM4, TIM_OC4, velocidad_der);


}
void Giro_amplio_CCW(void){

// para el duty hay que moverse entre los valores de 2250 0% Duty y 4800 100% duty

int velocidad_iz=2250+1000; //pongo 0 para indicar el incremento que no exceda los 4800 ojo
int velocidad_der=2250+2000; //pongo 0 para indicar el incremento que no exceda los 4800 ojo

timer_set_oc_value(TIM4, TIM_OC1, velocidad_iz);
timer_set_oc_value(TIM4, TIM_OC2, velocidad_der);
timer_set_oc_value(TIM4, TIM_OC3, velocidad_iz);
timer_set_oc_value(TIM4, TIM_OC4, velocidad_der);


}