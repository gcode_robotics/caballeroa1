#ifndef _DELAY_H_
#define _DELAY_H_

#include <libopencm3/stm32/gpio.h>
#include <libopencm3/stm32/rcc.h>
#include <libopencm3/stm32/timer.h>

#include "globales.h"

void delay_us(uint32_t us);

#endif