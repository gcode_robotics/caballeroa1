#ifndef _GLOBALES_H_
#define _GLOBALES_H_

// Velocidad máxima de reloj. En caso de la BluePill son 72MHz
#define FRECUENCIA_RELOJ_HZ 72000000
// Cantidad de microsegundos en un segundo
#define US_POR_S 1000000

//determina para el lado que queremos que gire
//HORARIO TRUE ANTIHORARIO FALSE
#define NUM_SENSORS 2
bool CCW;
bool CW;
 uint16_t sensors[NUM_SENSORS];
int Estado;


#endif