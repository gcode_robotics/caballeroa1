
#include "acciones.h"
/*
CONFIGURACION EN LOS PINES PARA COPIAR Y PEGAR SEGUN SE NECESITE
PARA POSIBLES CAMBIOS

B-> RIGHT MOTOR
A-> LEFT MOTOR

ADELANTE: CW B + CCW A
GIRO DERECHA (CW): CCW B + CCW A
GIRO IZQUIERDA (CCW): CW B + CW A
ATRAS: CCW B+ CW A

//CW A:
//AIN1-1
gpio_set(GPIOA,GPIO11);
gpio_set(GPIOA,GPIO6);
//AIN2-0
gpio_clear(GPIOA,GPIO12);
gpio_clear(GPIOA,GPIO5);

//CCW A:
//AIN1-0
gpio_clear(GPIOA,GPIO11);
gpio_clear(GPIOA,GPIO6);
//AIN2-1
gpio_set(GPIOA,GPIO12);
gpio_set(GPIOA,GPIO5);

//CW B:
//BIN1-1
gpio_set(GPIOA,GPIO15);
gpio_set(GPIOB,GPIO9);
//BIN2-0
gpio_clear(GPIOB,GPIO3);
gpio_clear(GPIOB,GPIO8);

//CCW B:
//BIN1-0
gpio_clear(GPIOA,GPIO15);
gpio_clear(GPIOB,GPIO9);
//BIN2-1
gpio_set(GPIOB,GPIO3);
gpio_set(GPIOB,GPIO8);



*/


//CONFIGURACIONES PARA EL TB6612FNG
 //////////CONTROL DIRECCION MOTOR////////////////


void adelante(void){
   
   //STBY
    gpio_set(GPIOB,GPIO15);
    gpio_set(GPIOB,GPIO4);
    
    //CW B:

    //Motor2  ESTE ES EL MOTOR QUE NO VA POR EL TEMA DE LOS PINES B3 Y A15
    //BIN1
    gpio_clear(GPIOA,GPIO15);
    //BIN2
    gpio_set(GPIOB,GPIO3);
    
    //motor 4 Delantero derecho
    //BIN1
    gpio_clear(GPIOA,GPIO8);
    //BIN2-0
    gpio_set(GPIOB,GPIO5);


    //Motor 1  trasero izquierdo
    //AIN1  
    gpio_set(GPIOA,GPIO11);//Atras Izquierda
    //AIN2
    gpio_clear(GPIOA,GPIO12);//Atras iizquierda
    
    //Motor 3 delantero izquierdo
    //CCW A:
    //AIN1-0
    gpio_set(GPIOB,GPIO10);
   //AIN2-1
   gpio_clear(GPIOB,GPIO11);

}

void giro_sobre_si_mismo_cw(void){
// RECORDAR AQUI ESTAN PUESTOS PARA IR ADELANTE, NO VALE
// HAY QUE CAMBIARLO Y RECALIBRAR YA QUE NO SE HA PODIDO POR EL EROR DE LOS PINES    
      //STBY
    gpio_set(GPIOB,GPIO15);
    gpio_set(GPIOB,GPIO4);
    
    //CW B:

    //Motor2  ESTE ES EL MOTOR QUE NO VA POR EL TEMA DE LOS PINES B3 Y A15
    //BIN1
    gpio_set(GPIOA,GPIO15);
    //BIN2
    gpio_clear(GPIOB,GPIO3);
    
    //motor 4 Delantero derecho
    //BIN1
    gpio_clear(GPIOA,GPIO8);
    //BIN2-0
    gpio_set(GPIOB,GPIO5);


    //Motor 1  trasero izquierdo
    //AIN1  
    gpio_set(GPIOA,GPIO11);//Atras Izquierda
    //AIN2
    gpio_clear(GPIOA,GPIO12);//Atras iizquierda
    
    
    //Motor 3 delantero izquierdo
    //CCW A:
    //AIN1-0
    gpio_set(GPIOB,GPIO10);
   //AIN2-1
   gpio_clear(GPIOB,GPIO11);
}

void giro_sobre_si_mismo_ccw(void){
    // RECORDAR AQUI ESTAN PUESTOS PARA IR ADELANTE, NO VALE
// HAY QUE CAMBIARLO Y RECALIBRAR YA QUE NO SE HA PODIDO POR EL EROR DE LOS PINES    
      //STBY
    gpio_set(GPIOB,GPIO15);
    gpio_set(GPIOB,GPIO4);
    
    //CW B:

    //Motor2  ESTE ES EL MOTOR QUE NO VA POR EL TEMA DE LOS PINES B3 Y A15
    //BIN1
    gpio_set(GPIOA,GPIO15);
    //BIN2
    gpio_clear(GPIOB,GPIO3);
    
    //motor 4 Delantero derecho
    //BIN1
    gpio_clear(GPIOA,GPIO8);
    //BIN2-0
    gpio_set(GPIOB,GPIO5);


    //Motor 1  trasero izquierdo
    //AIN1  
    gpio_set(GPIOA,GPIO11);//Atras Izquierda
    //AIN2
    gpio_clear(GPIOA,GPIO12);//Atras iizquierda
    
    
    //Motor 3 delantero izquierdo
    //CCW A:
    //AIN1-0
    gpio_set(GPIOB,GPIO10);
   //AIN2-1
   gpio_clear(GPIOB,GPIO11);
}

void hacia_atras(void){
   //STBY
    gpio_set(GPIOB,GPIO15);
    gpio_set(GPIOB,GPIO4);
    
    //CW B:

    //Motor2  ESTE ES EL MOTOR QUE NO VA POR EL TEMA DE LOS PINES B3 Y A15
    //BIN1
    gpio_set(GPIOA,GPIO15);
    //BIN2
    gpio_clear(GPIOB,GPIO3);
    
    //motor 4 Delantero derecho
    //BIN1
    gpio_set(GPIOA,GPIO8);
    //BIN2-0
    gpio_clear(GPIOB,GPIO5);


    //Motor 1  trasero izquierdo
    //AIN1  
    gpio_clear(GPIOA,GPIO11);//Atras Izquierda
    //AIN2
    gpio_set(GPIOA,GPIO12);//Atras iizquierda
    
    //Motor 3 delantero izquierdo
    //CCW A:
    //AIN1-0
    gpio_clear(GPIOB,GPIO10);
   //AIN2-1
   gpio_set(GPIOB,GPIO11);
}

/*|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||*/




//ACCIONES CONJUNTAS DETERMINANDO DIRECCION Y VELOCIDAD

//funcion para giro busqueda
void Buscar(void){
    // configura la direccion de giro conforme a la decision tomada
 if (CCW)
 {
    giro_sobre_si_mismo_ccw();
 }
 else { giro_sobre_si_mismo_cw();}
 
 //velocidad fija para el giro de busqueda
 giro_busqueda();

}

//funcion para persecucion
void Persigue(void){
adelante();
velocidad_motor();
}

//fucnion para atras
void Atras_que_me_salgo(void){
hacia_atras();
Atras();
}

//funcion giro amplio
void Giro_amplio(void){
    // configura la direccion de giro conforme a la decision tomada
    adelante();

    //seleccionamos el giro amplio usando variacion de las velocidades
 if (CCW)
 {
    Giro_amplio_CCW();
 }
 else { Giro_amplio_CW();}
 


}
