#ifndef __SETUP_H_
#define __SETUP_H_

#include <libopencm3/stm32/gpio.h>
#include <libopencm3/stm32/rcc.h>
#include <libopencm3/stm32/timer.h>

#include <libopencm3/stm32/flash.h>
#include <libopencm3/stm32/adc.h>
#include <libopencm3/stm32/usart.h>
#include <libopencm3/stm32/dma.h>


void setup (void);
void setup_PWM_timer(void);
void setup_clocks(void);
void setup_gpio(void);
void setup(void);
void usart_setup(void);
void my_usart_print_int(uint32_t, int);

#endif