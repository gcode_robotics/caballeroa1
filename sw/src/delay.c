#include "delay.h"

void delay_us(uint32_t us) {
  // Ticks actuales
  uint32_t initial_cycles = dwt_read_cycle_counter();
  // Cantidad de ticks que debe detener la ejecución en función de la frecuencia
  uint32_t sleep_cycles = (uint32_t)(FRECUENCIA_RELOJ_HZ * ((float)us / (float)US_POR_S));

  while (dwt_read_cycle_counter() - initial_cycles <= sleep_cycles) {
    //aqui pongo el ciclo de calibrado...
    
  };
}
